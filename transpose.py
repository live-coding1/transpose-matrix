# here we will loop the matrix to get all lines
# just to show you how to loop a matrix
# I will take a matrix with 4 length
def transpose_1(matrix):
    transposed = []
    for i in range(4):
        transposed_row = []
        for row in matrix:
            transposed_row.append(row[i])
        transposed.append(transposed_row)
    return transposed

# second method
def transpose_2(matrix):
    tranposed = []
    for i in range(4):
        tranposed.append([row[i] for row in matrix])
    return tranposed

# Third method
def transpose_3(matrix):
    return [[row[i] for row in matrix] for i in range(4)]

# fourth method
# so here I will the powerful of the zip method
def transpose_4(matrix):
    return list(zip(*matrix))

# Test cases
matrix = [[1, 2, 3, 4],
[5, 6, 7, 8],
[9, 10, 11, 12, 13]]

print(transpose_1(matrix))

print(transpose_2(matrix))

print(transpose_3(matrix))

print(transpose_4(matrix))

# All these methods are equivalents !

# If you like this video please share it and do a subscribe :)

# see you soon for other videos about Python for newbies